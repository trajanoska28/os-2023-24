# OS-2023-24

## Description

This repository is created for the purpose of the auditorium exercises for the subject Operating Systems 2023/2024.

## Prerequisites

 - [git](https://git-scm.com/)
 - Editor of your choice

## Instructions

To clone the repository to your local machine follow these instructions:

```
cd path_to_your_local_directory
git clone https://gitlab.com/trajanoska28/os-2023-24.git
```
