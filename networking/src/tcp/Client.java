package tcp;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

public class Client extends Thread {
    int serverPort;
    InetAddress serverAddress;

    public Client(int serverPort, InetAddress serverAddress) {
        this.serverPort = serverPort;
        this.serverAddress = serverAddress;
    }

    @Override
    public void run() {
        Random random = new Random();
        Socket socket = null;
        try {
            socket = new Socket(serverAddress, serverPort);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            String method = random.nextInt(10) % 2 == 0 ? "GET" : "POST";
            bufferedWriter.write(method + " /movies/" + random.nextInt(100) + " HTTP/1.1\n" );
            bufferedWriter.write("User: FINKI\n");
            bufferedWriter.write("\n");
            bufferedWriter.flush();

            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println("Received line from server: " + line);
            }
            System.out.println("done");
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws UnknownHostException {
        Client client = new Client(5000, InetAddress.getLocalHost());
        client.start();
    }
}
