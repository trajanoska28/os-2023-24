package tcp;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class TCPMain {
    public static void main(String[] args) throws InterruptedException, IOException {
        Server server = new Server(8000);
        server.start();
        Thread.sleep(1000);

        for (int i = 0; i < 10; i++) {
            Client client = new Client(8000, InetAddress.getLocalHost());
            client.start();
        }

        Thread.sleep(1000);

        // READ INPUT FROM THE CONSOLE AND SEND A REQUEST FROM THE MAIN THREAD
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String method, user;
        int resource;
        System.out.println("Enter method: GET/POST");
        method = br.readLine();
        System.out.println("Enter resource");
        resource = Integer.parseInt(br.readLine());
        System.out.println("Enter user");
        user = br.readLine();

        Socket socket = new Socket(InetAddress.getLocalHost(), 8000);
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        writer.write(method + " /movies/" + resource + " HTTP/1.1\n");
        writer.write("User: " + user + "\n");
        writer.flush();

        String line = reader.readLine();
        while (line != null && !line.equals("")) {
            System.out.println("Simulated request: " + line);
            line = reader.readLine();
        }
        socket.close();

        System.out.println("Main done");
    }
}
