package tcp;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Worker extends Thread {
    Socket socket;

    public Worker(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        BufferedReader bufferedReader = null;
        BufferedWriter bufferedWriter = null;
        try {
             bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

             Request request = new Request(bufferedReader.readLine().split(" "));
             System.out.println(request.method + " " + request.uri);
             String line;

             while ((line = bufferedReader.readLine()).equals("")) {
                    String[] parts = line.split(": ", 2);
                    request.headers.putIfAbsent(parts[0], parts[1]);
             }


             // SOME POST LOGIC
            // IF THE METHOD IS POST AND WE HAVE ADDED SOME CONTENT READ THE CONTENT AS BODY
             if (request.method.equals("POST") && request.headers.get("Content-Length") != null) {
                StringBuilder sb = new StringBuilder();
                int length = Integer.parseInt(request.headers.get("Content-Length").trim());
                while (length -- > 0) {
                    sb.append((char) bufferedReader.read());
                }

                request.body = sb.toString();
                System.out.println("BODY: " + request.body);
             }

             String clientName = request.headers.get("User");

             bufferedWriter.write("HTTP/1.1 OK\n");
             bufferedWriter.write("Hello " + clientName + "!\n");
             bufferedWriter.write("You request to " + request.method + " the resource: " + request.uri + "\n");
             bufferedWriter.flush();

             this.socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Request {
        String method;
        String uri;
        String version;
        Map<String, String> headers;
        String body;

        public Request(String[] line) {
            method = line[0];
            uri = line[1];
            version = line[2];
            headers = new HashMap<>();
        }
    }
}
