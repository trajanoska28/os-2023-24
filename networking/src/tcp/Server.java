package tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {
    private int port;

    Server(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        System.out.println("Server starting...");
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Server started");
        System.out.println("Waiting for connection...");

        while (true) {
            Socket client = null;
            try {
                client = serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("new client - creating new worker thread...");
            new Worker(client).start();
            System.out.println(client.getInetAddress() + " " + client.getPort());
        }
    }

    public static void main(String[] args) {
        Server server = new Server(5000);
        server.start();
    }
}
