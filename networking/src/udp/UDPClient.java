package udp;

import java.io.IOException;
import java.net.*;

public class UDPClient extends Thread {
        private String serverName;
        private int serverPort;
        private String message;

        private DatagramSocket socket;
        private InetAddress address;
        private byte[] buffer;

        public UDPClient(String serverName, int serverPort, String message) {
            this.serverName = serverName;
            this.serverPort = serverPort;
            this.message = message;
            buffer = message.getBytes();

            try {
                this.socket = new DatagramSocket();
                this.address = InetAddress.getByName(serverName);
            } catch (SocketException | UnknownHostException e) {
                e.printStackTrace();
            }
        }

    @Override
    public void run() {
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, serverPort);

        try {
            socket.send(packet);
            packet = new DatagramPacket(buffer, buffer.length, address, serverPort);
            socket.receive(packet);
            System.out.println("Client received: " + new String(packet.getData(), 0, packet.getLength()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        UDPClient client = new UDPClient("localhost", 6000, "Hello :)");
        client.start();
    }
}
