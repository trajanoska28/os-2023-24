import java.io.*;
import java.net.Socket;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;

public class Worker extends Thread {

    private final Socket clientSocket;
    private final String logFilePath;
    private static final Lock lock = new ReentrantLock();

    public Worker(Socket clientSocket, String logFilePath) {
        this.clientSocket = clientSocket;
        this.logFilePath = logFilePath;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(
                            clientSocket.getInputStream()
                    )
            );
            PrintWriter writer = new PrintWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(
                                    logFilePath,
                                    true
                            )
                    )
            );

            PrintWriter socketWriter = new PrintWriter(
                    new OutputStreamWriter(
                            clientSocket.getOutputStream()
                    )
            );

            String line;
            StringBuilder messages = new StringBuilder();
            //lock.lock(); // very slow
            while (!(line = reader.readLine()).isEmpty()) {
                // lock
                //writer.write(line + "\n"); // again very slow
                // unlock
                messages.append(line).append("\n");
            }
            //lock.unlock();
            System.out.println(messages.toString());

            lock.lock();
            writer.write(messages.toString());
            writer.flush();
            lock.unlock();

            socketWriter.write("200 OK");
            socketWriter.write("\n");
            socketWriter.flush();
            clientSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
