import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;

public class Server extends Thread {
    int port;
    String logFilePath;
    static long countConnections = 0;
    static Lock lock = new ReentrantLock();

    public Server(int port, String logFilePath) {
        this.port = port;
        this.logFilePath = logFilePath;
    }

    @Override
    public void run() {
        try {
            ServerSocket socket = new ServerSocket(port);
            while (true) {
                Socket socketClient = socket.accept();

                lock.lock();
                countConnections++;
                lock.unlock();

                System.out.println("New client...");
                // new Worker
                new Worker(socketClient, logFilePath).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //better to specify as environment variables
        Server server = new Server(8080, "data/logfile.txt");
        server.start();
    }
}
