import java.io.*;

public class SoundLevelMonitor {

    private static String getNoiseLevelFromValue(double value) {
        if (value == -1) return "NO DATA";
        if (value >= 80) return "HIGH";
        if (value >= 60) return "MEDIUM";
        return "LOW";
    }

    private static final int TIME_INTERVAL = 30000;

    public static void main(String[] args) {
        String inputFile = System.getenv("INPUT_FILE_PATH");
        String outputFile = System.getenv("OUTPUT_FILE_PATH");
        BufferedReader reader = null;
        FileWriter writer = null;

        try {
            reader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(inputFile)
                    ));
            writer = new FileWriter(outputFile, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(true) {

            try {
                Thread.sleep(TIME_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            double average = reader.lines()
                    .mapToInt(Integer::parseInt)
                    .average().orElse(-1);

            try {
                writer.write(getNoiseLevelFromValue(average) + "\n");
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
