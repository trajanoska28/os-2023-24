import java.io.*;
import java.util.Random;

public class SoundLevelSensor {

    private static final int TIME_INTERVAL = 20000;
    private static final int LOWER = 40;
    private static final int UPPER = 100;
    private static final int NUM_MEASURES = 10;

    public static void main(String[] args) {
        String filePath = System.getenv("FILE_PATH");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(filePath, true)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Random random = new Random();

        while (true) {

            // IF YOU WANT TO REWRITE THE FILE EACH TIME YOU ADD NEW MEASURE
            // place the initialization of the reader here without append

            //  writer = new PrintWriter(
            //          new OutputStreamWriter(
            //                  new FileOutputStream(filePath)));

            for (int i=0; i<NUM_MEASURES ; i++) {
                int number = random.nextInt(LOWER, UPPER);
                writer.write(number + "\n");
                writer.flush();
            }

            try {
                Thread.sleep(TIME_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
