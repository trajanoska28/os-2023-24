package shared;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LoggerWorker extends Thread {
    private final Socket socket;
    File logFile;
    File counterFile;
    private static final Lock logFileLock = new ReentrantLock();
    private static final Lock counterFileLock = new ReentrantLock();

    public LoggerWorker(Socket socket, String logFilePath, String counterFilePath) {
        this.socket = socket;
        this.logFile = new File(logFilePath);
        this.counterFile = new File(counterFilePath);
    }

    @Override
    public void run() {
        BufferedReader socketReader = null;
        BufferedWriter logWriter = null;
        RandomAccessFile counterFileRaf = null;
        try {
           socketReader = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()
                    )
            );
           logWriter = new BufferedWriter(
                   new OutputStreamWriter(
                           new FileOutputStream(
                                   logFile,
                                   true // append to the file because each thread would rewrite the content otherwise
                           )
                   )
           );
            counterFileRaf = new RandomAccessFile(counterFile, "rw");
        } catch (IOException e) {
            e.printStackTrace();
        }

        int localCurrentClientsCounter = 0;
        int fileCurrentClientCounter = 0;

        StringBuilder messagesToWrite = new StringBuilder();
        try {
            String line = null;
            while ((line = socketReader.readLine()) != null) {
                messagesToWrite.append(line).append("\n");
                localCurrentClientsCounter += 1;
            }

        }
        catch (IOException e) {
            e.printStackTrace();
        }

        // lock access to the shared log file
        // write all of the messages at once to be more efficient
        logFileLock.lock();
        try{
            logWriter.write(messagesToWrite.toString());
            logWriter.flush();
            logWriter.close();
            socketReader.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            logFileLock.unlock(); // this block always executes no matter if an exception occured or not
        }

        // RANDOM ACCESS FILE THROWS AN EXCEPTION IF IT'S EMPTY AND WE TRY TO READ AN INTEGER

        counterFileLock.lock(); // lock while we read the old value and unlock when the new value is written
        try {
            fileCurrentClientCounter = counterFileRaf.readInt(); // fileCurrentClientCounter would be 0 if no int
        } catch (IOException e) {
            e.printStackTrace();
        }

        try{
            System.out.println("Total number of clients currently: " + (localCurrentClientsCounter + fileCurrentClientCounter));
            counterFileRaf.seek(0); // set to the first character in the file
            counterFileRaf.writeInt(fileCurrentClientCounter + localCurrentClientsCounter); // write the new value
            counterFileRaf.close(); // close the file

            socket.close(); // close the socket since we're done
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            counterFileLock.unlock(); // unlock access to the random access file
            // this line will always be executed no matter what
        }

    }
}
