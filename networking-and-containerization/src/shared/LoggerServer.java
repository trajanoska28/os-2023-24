package shared;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class LoggerServer extends Thread {
    private final String csvFilePath;
    private final String counterFilePath;
    private final int port;

    public LoggerServer(String csvFilePath, String counterFilePath, int port) {
        this.csvFilePath = csvFilePath;
        this.counterFilePath = counterFilePath;
        this.port = port;
    }

    @Override
    public void run() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(this.port);
            System.out.println("Shared resource server started...");
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (true) {
            try {
                Socket socket = serverSocket.accept();
                System.out.println("New client...");
                // Start worker
                new LoggerWorker(socket, csvFilePath, counterFilePath).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        String csvFile = System.getenv("LOG_FILE");
        String counterFile = System.getenv("COUNTER_FILE");
        String port = System.getenv("SERVER_PORT");
        if (port == null) {
            throw new RuntimeException("Define port...");
        }

        LoggerServer server = new LoggerServer(
                csvFile,
                counterFile,
                Integer.parseInt(port)
        );
        server.start();
    }
}
